#requires -Modules Configuration, Pester, PSScriptAnalyzer

[CmdletBinding()]
[OutputType()]

$ErrorActionPreference = "Stop"

try {
    # Setup environment.
    Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Confirm:$false -Force | 
        Out-Null
    Install-Module -Repository PSGallery -Name Configuration, Pester, PSScriptAnalyzer -Confirm:$false -SkipPublisherCheck -Force | 
        Out-Null
    $env:PSModulePath += ";${env:CI_PUBLISH_DIR}"

    # Set working location to the root of the project directory.
    Set-Location -Path $env:CI_PROJECT_DIR
    Import-Module -Name ".\${env:CI_PROJECT_NAME}" -Force 

    $timestamp = Get-Date -uformat "%Y%m%d-%H%M%S"
    $resultsFile = "Results_${timestamp}.xml"

    Invoke-Pester -Path '.\Tests' -PassThru  | 
        Export-Clixml -Path ".\Pester_${resultsFile}"

    [Int]$failures = Import-Clixml -Path ".\Pester_${resultsFile}"  | 
        Select-Object -ExpandProperty FailedCount | 
            Measure-Object -Sum | 
                Select-Object -ExpandProperty Sum

    if ($failures -gt 0) {
        throw "Build failed."
    } else {
        if (Test-Path -Path "${env:CI_PUBLISH_DIR}\${CI_PROJECT_NAME}\${CI_PROJECT_NAME}.psd1") {
            $version = [Version](Get-Metadata -Path "${env:CI_PUBLISH_DIR}\${CI_PROJECT_NAME}\${CI_PROJECT_NAME}.psd1")
        } else {
            $version = [Version]::new(
                $env:CI_VERSION_MAJOR, $env:CI_VERSION_MINOR
            )
        }

        if ($env:CI_VERSION_MAJOR -lt $version.Major -or $env:CI_VERSION_MINOR -lt $version.Minor) {
            $version = [Version]::new(
                $env:CI_VERSION_MAJOR, $env:CI_VERSION_MINOR, 0
            )
        } else {
            $version = [Version]::new(
                $env:CI_VERSION_MAJOR, $env:CI_VERSION_MINOR, $version.Build + 1
            )
        }
        
        Update-Metadata -Path ".\${env:CI_PROJECT_NAME}\${env:CI_PROJECT_NAME}.psd1" -PropertyName ModuleVersion -Value $version.ToString()
        Copy-Item -Path ".\${env:CI_PROJECT_NAME}" -Destination "${env:CI_PUBLISH_DIR}" -Recurse -Force -Confirm:$false
    }
} catch {
    Write-Error $_
    exit 1
}
